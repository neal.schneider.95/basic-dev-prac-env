#!/bin/bash

# Check if brew is installed. If it is, update. If its not, install it (this may take a while).
which -s brew
if [[ $? != 0 ]] ; then
    # Install Homebrew
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
    brew update
fi

# Install python3.8-dev and python3-pip
# Using pyenv on mac is the correct way to set the python version.
brew install pyenv
pyenv install 3.8.5
pyenv global 3.8.5
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile

# Install gcc
# gcc@10 works on catalina, mojave and high sierra
brew install gcc@10

# Install python requirements
pip3 install -r requirements.txt