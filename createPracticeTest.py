#!/usr/bin/python3

import os, sys, subprocess, random

avoid_these = ['.DS_Store', 'VS_SLN_Template']

googletest_url = 'https://github.com/google/googletest.git'
basic_dev_url = 'https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file'
proj_path = os.path.abspath('./')
C_path = '.testbank/performance/C_Programming'
Python_path = '.testbank/performance/Python'
Networking_path = '.testbank/performance/Networking'

def cleanTest():
    if os.path.exists('C_Programming'):
        subprocess.call('rm -rf C_Programming'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.call('mkdir C_Programming'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if os.path.exists('Python'):
        subprocess.call('rm -rf Python'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.call('mkdir Python'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if os.path.exists('Networking'):
        subprocess.call('rm -rf Networking'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.call('mkdir Networking'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def getRandomQuestions():
    question_container = {
        'C': set(),
        'Python': set(),
        'Network': set()
    }

    while len(question_container['C']) < 2:
        C_question = random.choice(os.listdir(C_path))
        if C_question not in avoid_these and C_question not in question_container['C']:
            question_container['C'].add(C_question)

    while len(question_container['Python']) < 2:
        Python_question = random.choice(os.listdir(Python_path))
        if Python_question not in avoid_these and Python_question not in question_container['Python']:
            question_container['Python'].add(Python_question)

    question_container['Network'].add(random.choice(os.listdir(Networking_path)))
    return question_container

def copyQuestionToTest(questions):
    for question in questions['C']:
        subprocess.call(['cp', '-r', os.path.join(proj_path, C_path, question), 'C_Programming/'])
        if not os.path.exists(f'C_Programming/{question}/googletest'):
            subprocess.call(['cp', '-r', f'{proj_path}/googletest', f'C_Programming/{question}/'])
    for question in questions['Python']:
        subprocess.call(['cp', '-r', os.path.join(proj_path, Python_path, question), 'Python/'])
    for question in questions['Network']:
        subprocess.call(['cp', '-r', os.path.join(proj_path, Networking_path, question), 'Networking/'])


def main():
    cleanTest()
    
    if not os.path.exists('googletest'):
        subprocess.call(['git', 'clone', googletest_url])

    if not os.path.exists(".testbank"):
        subprocess.call(['git', 'clone', basic_dev_url, '.testbank'])
    
    copyQuestionToTest(getRandomQuestions())

    return 0

if __name__ == "__main__":
    main()
